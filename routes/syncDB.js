const express = require('express'),
    router = express.Router(),
    constants = require("../controller/constants"),
    croneJob = require("../controller/cronJob"),
    MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert'),
    mainServer = require("../controller/sendInfoToMainServer"),
    pushNotification = require('../controller/firebasePushMessage'),
    AssertionError = require('assert').AssertionError;

router.get('/users', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            const db = client.db(constants.mongodbName);

            findUsers(db, (err, docs) => {
                client.close();
                res.json(docs);
                res.end();
            });
        } catch (err) {
            client.close();
            res.end();
        }
    });
});

router.post('/user', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            let newUser = {
                username: req.body.username,
                password: req.body.password,
                name: req.body.name,
                lastName: req.body.lastName,
                rank: req.body.rank,
                // macAddressDevice: "",
                // tokenPushNotification: "",
                doorPermission: false
            };

            insertUser(db, newUser, (err, results) => {
                client.close();
                res.status(err ? 503 : 200).json({
                    error: err,
                    message: 'Request completed'
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                error: true,
                message: 'Request completed'
            });
            res.end();
        }
    });
});

router.post('/users', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            insertUsers(db, req.body, (err, results) => {
                client.close();
                res.status(err ? 503 : 200).json({
                    error: err,
                    message: 'Request completed'
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                error: true,
                message: 'Request completed'
            });
            res.end();
        }
    });
});

router.put('/user', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            // let newUser = {
            //     username: req.body.username,
            //     password: req.body.password,
            //     name: req.body.name,
            //     lastName: req.body.lastName,
            //     rank: req.body.rank,
            //     // macAddressDevice: "",
            //     // tokenPushNotification: "",
            //     doorPermission: false
            // };

            updateUser(db, req.body, (err, results) => {
                client.close();
                res.status(err ? 503 : 200).json({
                    error: err,
                    message: 'Request completed'
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                error: true,
                message: 'Request completed'
            });
            res.end();
        }
    });
});

router.delete('/user', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            removeUser(db, req.body.username, (err, results) => {
                client.close();
                res.status(err ? 503 : 200).json({
                    error: err,
                    message: 'Request completed'
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                error: true,
                message: 'Request completed'
            });
            res.end();
        }
    });
});

router.put('/config', (req, res, next) => {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            updateRouterMac(db, String(req.body.routerMacAddress).valueOf().toUpperCase(), (err, results) => {
                client.close();
                res.status(err ? 503 : 200).json({
                    error: err,
                    message: 'Request completed'
                });
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                error: true,
                message: 'Request completed'
            });
            res.end();
        }
    });
});

router.get('/report', (req, res, next) => {
    croneJob.sendReport((err, data) => {
        res.status(err ? 503 : 200).json({
            error: err,
            data,
            message: 'Request completed'
        });
        res.end();
    });
});

/**
 * @param
    {
            "username": "aleksandar.vukasinovic",
            "macAddressDevice": "2018-03-17T19:54:02.386Z",
            "dateEnd": "2018-03-17T19:54:02.386Z"
        }
 */
router.post('/syncMac', (req, res, next) => {
    mainServer.sendUnregisteredDeviceWaitMode({
        username: req.body.username,
        macAddressDevice: req.body.macAddressDevice
    }, (err, data) => {
        res.status(err ? 503 : 200).json({
            err: err,
            data,
            uniqueToken: constants.uniqueToken,
            username: req.body.username,
            macAddressDevice: req.body.macAddressDevice
        });
        res.end();
    });
});

/**
 * @param
    {
            "username": "aleksandar.vukasinovic",
            "dateStart": "2018-03-17T19:54:02.386Z",
            "dateEnd": "2018-03-17T19:54:02.386Z"
        }
 */
router.post('/getTimeForUser', (req, res, next) => {
    mainServer.sendUnregisteredDeviceWaitMode({
        username: req.body.username,
        dateStart: req.body.dateStart,
        dateEnd: req.body.dateEnd
    }, (err, data) => {
        res.status(err ? 503 : 200).json({
            err: err,
            data,
            username: req.body.username,
            dateStart: req.body.dateStart,
            dateEnd: req.body.dateEnd
        });
        res.end();
    });
});

module.exports = router;

const findUsers = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.find({}).toArray((err, docs) => {
        try {
            assert.equal(err, null);
            // console.log("Inserted user in collection");
            return callback(false, docs);
        } catch (e) {
            return callback(true, docs);
        }
    });
};

const insertUser = function (db, user, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);

    checkUserExists(db, user, (exist) => {
        if (exist) {
            collection.updateOne({username: user.username}
                , {$set: user}, function (err, result) {
                    try {
                        assert.equal(err, null);
                        return callback(false, result);
                    } catch (e) {
                        return callback(true, result);
                    }
                });
        } else {
            collection.insertOne(user, (err, result) => {
                try {
                    assert.equal(err, null);
                    // console.log("Inserted user in collection");
                    return callback(false, result);
                } catch (e) {
                    return callback(true, result);
                }
            });
        }
    });

};

const insertUsers = function (db, users, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);

    users.filter((user) => {
        checkUserExists(db, user, (exist) => {
            if (exist) {
                collection.updateOne({username: user.username}
                    , {$set: user}, function (err, result) {
                        try {
                            assert.equal(err, null);
                            return callback(false, result);
                        } catch (e) {
                            return callback(true, result);
                        }
                    });
            } else {
                collection.insertOne(user, (err, result) => {
                    try {
                        assert.equal(err, null);
                        // console.log("Inserted user in collection");
                        return callback(false, result);
                    } catch (e) {
                        return callback(true, result);
                    }
                });
            }
        });
    });


    // collection.insertMany(users, (err, result) => {
    //     try {
    //         assert.equal(err, null);
    //         // console.log("Inserted users in collection");
    //         return callback(false, result);
    //     } catch (e) {
    //         // if (e instanceof AssertionError) {
    //         //     console.log(e);
    //         // } else {
    //         //     console.log(e);
    //         // }
    //         return callback(true, result);
    //     }
    // });
};

const checkUserExists = function (db, user, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.findOne({username: user.username}, (err, docUser) => {
        if (docUser) {
            return callback(true);
        } else {
            return callback(false);
        }
    });
};

// async function checkUserExists(db, user) {
//     const collection = await db.collection(constants.mongodbCollectionStudents);
//     try {
//         let userCount = (await collection.findOne({username: user.username}).limit(1).count());
//         return userCount > 0;
//     } finally {
//         db.close();
//     }
// }

const updateUser = function (db, user, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    changeDoorPermissionUser(db, user, () => {
        collection.updateOne({username: user.username}
            , {$set: user}, function (err, result) {
                try {
                    assert.equal(err, null);
                    return callback(false, result);
                } catch (e) {
                    return callback(true, result);
                }
            });
    });
};

const removeUser = function (db, username, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.deleteOne({username: username}, (err, result) => {
        try {
            assert.equal(err, null);
            return callback(false, result);
        } catch (e) {
            return callback(true, result);
        }
    });
};

const changeDoorPermissionUser = function (db, user, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.findOne({username: user.username}, function (err, dbUser) {
        try {
            assert.equal(err, null);
            // console.log("Found the following records");
            if (dbUser.doorPermission === false && user.doorPermission === true) {
                console.log("PUSH");
                pushNotification.fcmSendMessageDate(
                    dbUser.tokenPushNotification,
                    {brava: "true"},
                    "Lab Access Control", "Vaš nalog je aktiviran od strane administratora.",
                    'Notification_Info');
            }
        } catch (e) {

        }
        return callback();
    });
};

const updateRouterMac = function (db, macAddress, callback) {
    const collection = db.collection(constants.mongodbCollectionSettings);
    checkRouterConfigExists(db, (exists) => {
        if (exists) {
            collection.updateOne({config: "router"}, {
                $set: {
                    samsungAppsLabRouterMacAddress: String(macAddress).valueOf().toUpperCase()
                }
            }, function (err, result) {
                try {
                    assert.equal(err, null);
                    return callback(false, result);
                } catch (e) {
                    return callback(true, result);
                }
            });
        } else {
            collection.insertOne({
                config: "router",
                samsungAppsLabRouterMacAddress: String(macAddress).valueOf().toUpperCase()
            }, (err, result) => {
                try {
                    assert.equal(err, null);
                    return callback(false, result);
                } catch (e) {
                    return callback(true, result);
                }
            });
        }
    });
};

const checkRouterConfigExists = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionSettings);
    collection.find({config: "router"}).toArray(function (err, docs) {
        try {
            assert.equal(err, null);
            if (docs.length > 0) {
                return callback(true);
            } else {
                return callback(false);
            }
        } catch (e) {
            return callback(false);
        }
    });
};