const express = require('express'),
    router = express.Router(),
    constants = require("../controller/constants"),
    MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert'),
    pushNotification = require('../controller/firebasePushMessage'),
    AssertionError = require('assert').AssertionError,
    mainServer = require("../controller/sendInfoToMainServer"),
    bcrypt = require('bcryptjs'),
    crypto = require('crypto');

/**
 * @deprecated url: /LogInLab
 * @new url : login
 */
router.post('/login', function (req, res) {
    //console.log("PRIMIO PORUKU LOG IN LAB");
    // let hashPass = require('crypto').createHash('md5').update(pass).digest('hex');
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            const db = client.db(constants.mongodbName);
            checkLoginUserExists(db, req.body, (err, data) => {
                client.close();
                res.status(200).json(data);
                res.end();
            });
        } catch (err) {
            res.status(503).json({
                error: true,
                message: 'Request completed'
            });
            res.end();
        }
    });
});

/**
 * @desc Ocekuje prazan POST,
 * @return niz prisutnih usera u laboratoriji
 *
 * @example
 [
 {
     "ime": "Dimitrije",
     "prezime": "Dimitrijevic",
     "zvanje": "stud"
 },
 {
     "ime": "Pera",
     "prezime": "Peric",
     "zvanje": "stud"
 }
 ]

 @deprecated url: /zahtev_prisutni
 @new url: /available-user
 */
router.post('/available-user', function (req, res) {
    if (req.body.uniqueToken === constants.uniqueToken) {
        MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
            try {
                assert.equal(null, err);
                const db = client.db(constants.mongodbName);

                getAllAvailableUserInLab(db, (err, array) => {
                    let availableUsers = [];
                    if (array && array.length > 0) {
                        array.filter((user) => {
                            availableUsers.push({
                                'ime': user.name,
                                'prezime': user.lastName,
                                'zvanje': user.rank
                            });
                        });
                        client.close();
                        res.status(200).json(availableUsers);
                        res.end();
                    }
                });
            } catch (err) {
                res.status(503).json({
                    error: true,
                    message: 'Request completed'
                });
                res.end();
            }
        });
    }
});

/**
 * @desc putanja ocekuje novi firebase token od mobilnog telefona
 * i vezuje taj token za device mac address-u user-a
 *
 * @deprecated url:  /firebaseToken
 * @new url : /firebase
 */
router.post('/firebase', function (req, res) {
    let deviceInfo = req.body;
    if (deviceInfo.uniqueToken === constants.uniqueToken && deviceInfo.tokenPushNotification !== null) {
        MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
            try {
                assert.equal(null, err);
                const db = client.db(constants.mongodbName);

                updateFireBaseToken(db, deviceInfo, (err, results) => {
                    client.close();
                    res.status(err ? 503 : 200).json({
                        error: err,
                        message: 'Request completed',
                        firebaseToken: err ? 'nijeupisan' : 'upisan'
                    });
                    res.end();
                });
            } catch (err) {
                res.end();
            }
        });
    } else {
        res.status(503).json({
            firebaseToken: 'nijeupisan'
        });
        res.end();
    }
});

/**
 * @deprecated url: /zahtev_vreme
 * @new url: /my-time
 * @param
    {
        "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
        "macAddressDevice":"",
        "dateStart":""
        "dateEnd":""
    }
 */
router.post('/my-time', function (req, res) {
    //console.log("Primljen zahtev za vreme");
    if (req.body.uniqueToken === constants.uniqueToken) {
        MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
            try {
                assert.equal(null, err);
                const db = client.db(constants.mongodbName);

                getTimeRangeForUser(db, req.body, (err, results) => {
                    client.close();
                    res.status(200).json({err, results});
                    res.end();
                });
            } catch (err) {
                res.status(503).json(err);
                res.end();
            }
        });
    } else {
        res.status(503).json(false);
        res.end();
    }
});

/**
 * getAllUse:
 * @param
 * {
    "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
    "action": "getUsers"
    }
 * sendMessage
 * @param
 *{
     "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
     "action": "sendMessage",
     "receivers":[
            	"mitad92@gmail.com",
            	"pera@gmail.com",
            	"alex@gmail.com"
            	],
     "sender":"mitad92@gmail.com",
    "senderName":"Dimitrije DImitrijevic",
    "message":"message"
    }
 *
 *getMyMessage
 * @param
 * {
            "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
            "action": "getMyMessage",
            "receiver":"mitad92@gmail.com"
    }
 *
 * removeMyMessage
 * @param
 * {
            "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
            "action": "removeMyMessage",
            "idMessage":[
            "5aad93ecdc94291dc4543c70",
            "5aad94509d91e32054671a64"
            ]
}
 */
router.post('/chat', function (req, res) {
    if (req.body.uniqueToken === constants.uniqueToken) {
        MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
            try {
                assert.equal(null, err);
                const db = client.db(constants.mongodbName);

                if (req.body.action === "getUsers") {
                    getAllUserWhenHaveNotificationTokenChat(db, (err, data) => {
                        res.status(200).json(data);
                        res.end();
                    });
                } else if (req.body.action === "sendMessage") {
                    sendMessageChat(db, req.body, (err, data) => {
                        res.status(200).json({
                            odgovor: 'PorukaPrimljena'
                        });
                        res.end();
                    });
                } else if (req.body.action === "getMyMessage") {
                    getAllMyMessageChat(db, req.body, (err, data) => {
                        res.status(200).json(data);
                        res.end();
                    });
                } else if (req.body.action === "removeMyMessage") {
                    deleteMyMessageChat(db, req.body, (err, data) => {
                        res.status(200).json({
                            success: 'messagesRemove'
                        });
                        res.end();
                    });
                }
            } catch (err) {
                res.end();
            }
        });
    } else {
        res.status(503).json(false);
        res.end();
    }
});

module.exports = router;

const checkLoginUserExists = function (db, user, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    const collectionSettings = db.collection(constants.mongodbCollectionSettings);
    collection.findOne({username: user.username}, (err, doc) => {
        try {
            assert.equal(err, null);
            if (doc) {
                let hashPass = crypto.createHash('md5').update(user.password).digest('hex');
                // bcrypt.compare(user.password, doc.password, function (err, res) {
                // res == true
                if (String(hashPass).valueOf() === String(doc.password).valueOf()) {
                    // if (res) {
                    /**
                     * @desc ispravan password
                     */
                    if (matchMacDevice(user.macAddressDevice, doc.macAddressDevice)) {
                        if (!doc.doorPermission) {
                            //TODO login ali u weit mode, ceka se odobrenje admina
                            //TODO send data username, deviceMac na server na odobrenje
                            /**
                             * @desc login ali u weit mode, ceka se odobrenje admina
                             * send data username, deviceMac na server na odobrenje
                             */
                            mainServer.sendUnregisteredDeviceWaitMode({
                                username: user.username,
                                macAddressDevice: user.macAddressDevice
                            });
                        }
                        //TODO USPESAN LOGIN, sve dozvoljeno
                        /**
                         * @desc USPESAN LOGIN, sve dozvoljeno
                         */
                        collectionSettings.findOne({config: "router"}, (err, docSettings) => {
                            return callback(false, {
                                err: false,
                                message: "success",
                                doorPermission: doc.doorPermission,
                                username: doc.username,
                                name: doc.name,
                                lastName: doc.lastName,
                                rank: doc.rank,
                                samsungAppsLabRouterMacAddress: docSettings.samsungAppsLabRouterMacAddress,
                                uniqueToken: constants.uniqueToken
                            });
                        });
                    } else {
                        /**
                         * @desc Neovlascen uredjaj za tog usera
                         */
                        return callback(false, {
                            err: false,
                            message: 'notValidMacAddress'
                        });
                    }
                } else {
                    /**
                     * @desc neispravan password
                     */
                    return callback(false, {
                        message: 'notValidPassword'
                    });
                }
                // });
            } else {
                /**
                 * @desc neispravan username
                 */
                return callback(false, {
                    message: 'notValidUsername'
                });
            }
        } catch (e) {
            /**
             * @desc neispravan username ili greska
             */
            return callback(true, {
                message: 'notValidError'
            });
        }
    });
};

const matchMacDevice = function (mac, macInDb) {
    if (macInDb) {
        return (String(macInDb).valueOf().toUpperCase() === String(mac).valueOf().toUpperCase());
    } else {
        return true;
    }
};

const checkMacDeviceAddressExists = function (db, deviceInfo, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.find({macAddressDevice: deviceInfo.macAddressDevice}).toArray(function (err, docs) {
        try {
            assert.equal(err, null);
            if (docs.length > 0) {
                return callback(true);
            } else {
                return callback(false);
            }
        } catch (e) {
            return callback(false);
        }
    });
};

const updateFireBaseToken = function (db, deviceInfo, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.updateOne({username: deviceInfo.username}, {
        $set: {
            tokenPushNotification: String(deviceInfo.tokenPushNotification).valueOf()
        }
    }, function (err, result) {
        try {
            assert.equal(err, null);
            return callback(false, result);
        } catch (e) {
            return callback(true, result);
        }
    });
};

const getAllAvailableUserInLab = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionAvailable);
    collection.find().toArray(function (err, docs) {
        try {
            assert.equal(err, null);
            if (docs.length > 0) {
                return callback(true, docs);
            } else {
                return callback(false);
            }
        } catch (e) {
            return callback(false);
        }
    });
};

const getTimeRangeForUser = function (db, data, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.findOne({macAddressDevice: data.macAddressDevice}, function (err, user) {
        try {
            assert.equal(err, null);
            if (user) {
                mainServer.getUserTimeInLab({
                    uniqueToken: constants.uniqueToken,
                    username: user.username,
                    dateStart: data.dateStart,
                    dateEnd: data.dateEnd
                }, (err, req) => {
                    return callback(err, req);
                });
                // return callback(true, docs);
            } else {
                return callback(true);
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const getAllUserWhenHaveNotificationTokenChat = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.find({}).toArray(function (err, users) {
        try {
            assert.equal(err, null);
            if (users) {
                let dataSortUser = [];

                users.filter((user) => {
                    if (user && user.tokenPushNotification) {
                        dataSortUser.push({
                            ime: user.name,
                            prezime: user.lastName,
                            zvanje: user.rank,
                            username: user.username
                        })
                    }
                });
                return callback(false, dataSortUser);
            } else {
                return callback(true);
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const sendMessageChat = function (db, data, callback) {
    const collection = db.collection(constants.mongodbCollectionChat);
    const collectionUsers = db.collection(constants.mongodbCollectionStudents);
    if (data.receivers) {
        data.receivers.filter((receiver, index, array) => {
            collection.insertOne({
                sender: data.sender,
                senderName: data.senderName,
                receiver: receiver,
                message: data.message,
                date: new Date()
            }, (err, res) => {
                try {
                    collectionUsers.findOne({username: receiver}, (errSend, userSend) => {
                        try {
                            pushNotification.fcmSendMessageDate(
                                userSend.tokenPushNotification,
                                {chat: 'true'},
                                data.senderName,
                                data.message,
                                'Notification_Info');
                            if (index === array.length - 1) {
                                callback(false);
                            }
                        } catch (errSend) {
                            if (index === array.length - 1) {
                                callback(true);
                            }
                        }
                    });
                } catch (err) {
                    if (index === array.length - 1) {
                        callback(true);
                    }
                }
            });
        });
    }
};

const getAllMyMessageChat = function (db, data, callback) {
    const collection = db.collection(constants.mongodbCollectionChat);
    collection.find({receiver: data.receiver}).toArray(function (err, messages) {
        try {
            assert.equal(err, null);
            if (messages) {
                let allMessages = [];
                messages.filter((message) => {
                    if (message) {
                        allMessages.push({
                            idMessage: message._id,
                            // sender: message.sender,
                            senderName: message.senderName,
                            // receiver: message.receiver,
                            message: message.message,
                            dateDate: new Date(message.date).dateInLab(),
                            dateTime: new Date(message.date).timeSpentInLab(),
                        })
                    }
                });
                return callback(false, allMessages);
            } else {
                return callback(true);
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const deleteMyMessageChat = function (db, data, callback) {
    const collection = db.collection(constants.mongodbCollectionChat);
    if (data) {
        data.idMessage.filter((id, index, array) => {
            collection.deleteOne({_id: ObjectId(id)}, (err, result) => {
                try {
                    assert.equal(err, null);
                    if (index === array.length - 1) {
                        return callback(false, result);
                    }
                } catch (e) {
                    if (index === array.length - 1) {
                        return callback(true, result);
                    }
                }
            });
        });
    }
};

Date.prototype.timeSpentInLab = function (data) {
    let d;
    if (data) {
        d = new Date(data);
    } else {
        d = this;
    }
    let seconds = d.getSeconds();
    let minutes = d.getMinutes();
    let hours = this.getUTCHours();
    return hours + ":" + minutes
};

Date.prototype.dateInLab = function (data) {
    let d;
    if (data) {
        d = new Date(data);
    } else {
        d = this;
    }
    let year = d.getFullYear();
    let month = d.getMonth() + 1;
    let day = ('0' + d.getDate()).slice(-2);
    return day + "." + month + "." + year;
};