const constants = require("../controller/constants");
let gpio;
if (!constants.developedMode) {
    gpio = require("pi-gpio");
}
// const io = require('socket.io')(constants.socketIOPort);
const io = require('socket.io');

const MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert');

module.exports.setServer = function (server) {
    let socketIo = io.listen(server);

    socketIo.on('connection', function (socket) {
        //console.log("User " + socket.handshake.address + " connected!");

        /**
         *OTKLUCAVA VRATA,
         *Pobudjuje pin 11, pomocu biblioteke "pi-gpio"
         * * @param
         * {
            "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
            "action":"unlock"
        }
     */
        socket.on('unlock', function (data) {
            let reqData;
            try {
                if (typeof data === "object") {
                    reqData = data;
                } else {
                    reqData = JSON.parse(data);
                }
            } catch (er) {

            }
            if (reqData.action === "unlock" && reqData.uniqueToken === constants.uniqueToken) {
                if (!constants.developedMode) {
                    gpio.open(11, "output", function (err) {
                        gpio.write(11, 1, function () {
                            socketIo.to(socket.id).emit('unlocked', constants.durationOpenDoor);
                            setTimeout(function () {
                                gpio.close(11);
                            }, constants.durationOpenDoor);
                        });
                    });
                } else {
                    socketIo.to(socket.id).emit('unlocked', constants.durationOpenDoor);
                }
            } else {
                socketIo.to(socket.id).emit('error', true);
            }
        });

        /**
         *Prijavljuje/Odjavljuje studenta iz laboratorije
         * @param
         * {
            "uniqueToken":"D/6H{x7ueK?%[w/2p4_G_*V",
            "action":"checkIN",
            "macAddressDevice":"11:11:22:33:44:AC"
        }
     */
        socket.on('check', function (data) {
            let reqData;
            try {
                if (typeof data === "object") {
                    reqData = data;
                } else {
                    reqData = JSON.parse(data);
                }
            } catch (er) {

            }
            if (reqData.uniqueToken === constants.uniqueToken) {
                MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
                    try {
                        assert.equal(err, null);
                        // //console.log("Connected successfully to server");
                        const db = client.db(constants.mongodbName);

                        if (String(reqData.action).valueOf() === "checkIN") {
                            checkInLaboratory(db, reqData, (err) => {
                                client.close();
                                if (err) {
                                    socketIo.to(socket.id).emit('error', true);
                                } else {
                                    socketIo.to(socket.id).emit('checkRequest', "checkIN");
                                }
                            });
                        } else if (reqData.action === "checkOUT") {
                            checkoutLaboratory(db, reqData, (err) => {
                                client.close();
                                if (err) {
                                    socketIo.to(socket.id).emit('error', true);
                                } else {
                                    socketIo.to(socket.id).emit('checkRequest', "checkOUT");
                                }
                            });
                        }
                    } catch (err) {
                    }
                });
            } else {
                socketIo.to(socket.id).emit('error', true);
            }
        });

        /**
         *Za odredjenog studenta brise njegovu mac adresu i firebase token njegov, i time ga odjavljuje iz baze
         *i za ponovno logovanje ceka odobrenje od studenta.
         */
        socket.on('logout', function (data) {
            let reqData;
            try {
                if (typeof data === "object") {
                    reqData = data;
                } else {
                    reqData = JSON.parse(data);
                }
            } catch (er) {

            }
            if (reqData.uniqueToken === constants.uniqueToken) {
                MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
                    try {
                        assert.equal(null, err);
                        const db = client.db(constants.mongodbName);
                        logOutLaboratory(db, reqData, (err) => {
                            client.close();
                            socketIo.to(socket.id).emit('logout', "success");
                        });
                    } catch (err) {

                    }
                });
            } else {
                socketIo.to(socket.id).emit('logout', false);
            }
        });
    });

};

const checkInLaboratory = function (db, data, callback) {
    const collectionUser = db.collection(constants.mongodbCollectionStudents);
    const collectionAvailable = db.collection(constants.mongodbCollectionAvailable);
    let checkInTime = new Date();
    let checkin;
    let existsCheckIn = false;
    let userForFindQuery;
    let updateQuery;
    collectionUser.findOne({macAddressDevice: data.macAddressDevice}, (err, user) => {
        try {
            assert.equal(err, null);
            if (user) {
                if (user.timeInLab !== null) {
                    user.timeInLab.filter((time) => {
                        if (+new Date(time.dateCheckInUnique).withoutTime() === +checkInTime.withoutTime()) {
                            existsCheckIn = true;
                            checkin = time;
                        } else {
                            existsCheckIn = false;
                        }
                    });
                }

                if (existsCheckIn) {
                    userForFindQuery = {
                        username: user.username,
                        // 'timeInLab.dateCheckIn': checkInTime.dateInLab(),
                        'timeInLab.dateCheckInUnique': {
                            "$gte": checkInTime.withoutTime(),
                            "$lt": checkInTime.addDay(1).withoutTime()
                        }
                    };
                    updateQuery = {$set: {'timeInLab.$.dateCheckInUnique': checkInTime}};
                } else {
                    userForFindQuery = {
                        username: user.username
                    };
                    checkin = {
                        // dateCheckIn: checkInTime.dateInLab(),
                        dateCheckInUnique: checkInTime,
                        totalTimeForDay: 0
                    };
                    updateQuery = {$push: {timeInLab: checkin}};
                }

                collectionAvailable.insertOne({
                    name: user.name,
                    lastName: user.lastName,
                    rank: user.rank,
                    macAddressDevice: user.macAddressDevice,
                    ipAddressDevice: data.ipAddressDevice
                }, (err, doc) => {
                    try {
                        assert.equal(err, null);
                        collectionUser.findOneAndUpdate(userForFindQuery, updateQuery, (errCheckIn, userCheckIn) => {
                            try {
                                assert.equal(errCheckIn, null);
                                return callback(false);
                            } catch (errCheckIn) {
                                return callback(true);
                            }
                        });
                    } catch (err) {
                        return callback(true);
                    }
                });
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const checkoutLaboratory = function (db, data, callback) {
    const collectionUser = db.collection(constants.mongodbCollectionStudents);
    const collectionAvailable = db.collection(constants.mongodbCollectionAvailable);
    let checkOutTime = new Date();
    let checkout;
    collectionUser.findOne({macAddressDevice: data.macAddressDevice}, (err, user) => {
        try {
            assert.equal(err, null);
            if (user) {
                user.timeInLab.filter((time) => {
                    if (new Date(time.dateCheckInUnique).withoutTime().valueOf() === checkOutTime.withoutTime().valueOf()) {
                        checkout = time;
                    }
                    // if (time.dateCheckIn === checkOutTime.dateInLab()) {
                    //     checkout = time;
                    // }
                });

                collectionAvailable.deleteOne({macAddressDevice: user.macAddressDevice}, (err, doc) => {
                    try {
                        assert.equal(err, null);
                        collectionUser.findOneAndUpdate({
                            username: user.username,
                            // 'timeInLab.dateCheckIn': checkOutTime.dateInLab()
                            'timeInLab.dateCheckInUnique': {
                                "$gte": checkOutTime.withoutTime(),
                                "$lt": checkOutTime.addDay(1).withoutTime()
                            }
                        }, {$set: {'timeInLab.$.totalTimeForDay': checkSumTimeInLab(checkout.dateCheckInUnique, checkOutTime, checkout.totalTimeForDay)}}, (errCheckOutUser, userCheckOut) => {
                            try {
                                assert.equal(errCheckOutUser, null);
                                callback(false);
                            } catch (errCheckOutUser) {
                                return callback(true);
                            }
                        });
                    } catch (err) {
                        return callback(true);
                    }
                });
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const logOutLaboratory = function (db, data, callback) {
    const collectionUser = db.collection(constants.mongodbCollectionStudents);
    collectionUser.findOneAndUpdate({macAddressDevice: data.macAddressDevice}, {$set: {tokenPushNotification: null}}, (err, user) => {
        try {
            assert.equal(err, null);
            return callback(false);
        } catch (err) {
            return callback(true);
        }
    });
};

Date.prototype.addDay = function (numberDay) {
    let d = new Date(this);
    d.setDate(this.getDate() + numberDay);
    return d;
};

Date.prototype.withoutTime = function () {
    let d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
};

let checkSumTimeInLab = function (dateStart, dateEnd, currentTotalTime) {
    return Number(new Date(currentTotalTime).getTime()) + (Number(new Date(dateEnd).getTime()) - Number(new Date(dateStart).getTime()));
};
