const constants = require("./constants"),
    MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert'),
    schedule = require('node-schedule'),
    mainServer = require("./sendInfoToMainServer");

module.exports.cronJobStart = function () {
    schedule.scheduleJob('* 21 * * *', function () {
        //TODO REPORT TIME SERVERU
        return send((err) => {
            // console.log(err);
        });
    });
    schedule.scheduleJob('* * 28 * *', function () {
        MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
            try {
                assert.equal(null, err);
                // console.log("Connected successfully to server");
                const db = client.db(constants.mongodbName);

                deleteAllChatMessage(db, (err, res) => {
                    // console.log("All REMOVE");
                });
            } catch (err) {

            }
        });
    });
};

module.exports.sendReport = function (callback) {
    return send(callback);
};

const send = function (callback) {
    MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
        try {
            assert.equal(null, err);
            // console.log("Connected successfully to server");
            const db = client.db(constants.mongodbName);

            getReportTimeInLab(db, (err, data) => {
                //TODO SEND
                // client.close();
                mainServer.sendTodayReportTimeInLab(data, (err, res) => {
                    if (err) {
                        //TODO ne brism izvestaje iz baze
                        callback(err, res);
                    } else {
                        //TODO  brism izvestaje iz baze
                        deleteAllReportTimeInLab(db, (err) => {
                            client.close();
                            callback(err, res);
                        });
                    }
                });
            });
        } catch (err) {
            callback(true);
        }
    });
};

const getReportTimeInLab = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.find({}).toArray(function (err, users) {
        try {
            assert.equal(err, null);
            if (users.length > 0) {
                let allTimeReport = [];
                let reportTimeInLab = [];
                users.filter((user) => {
                    reportTimeInLab = [];

                    if (user.timeInLab && user.timeInLab.length > 0) {
                        user.timeInLab.filter((time) => {
                            reportTimeInLab.push({
                                dateCheckInUnique: new Date(time.dateCheckInUnique).formatServer(),
                                totalTimeForDay: time.totalTimeForDay
                            });
                        });

                        allTimeReport.push({
                            username: user.username,
                            timeInLab: reportTimeInLab
                        });
                    }
                });
                callback(false, allTimeReport);
            } else {
                return callback(false);
            }
        } catch (err) {
            return callback(true);
        }
    });
};

const deleteAllReportTimeInLab = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.findOneAndUpdate({}, {
        $set: {timeInLab: []}
    }, function (err, users) {
        try {
            assert.equal(err, null);
            return callback(false);
        } catch (err) {
            return callback(true);
        }
    });
};

const deleteAllChatMessage = function (db, callback) {
    const collection = db.collection(constants.mongodbCollectionChat);
    collection.removeMany({}, (err, res) => {
        callback(err, res);
    });
};

Date.prototype.formatServer = function (data) {
    let d;
    if (data) {
        d = new Date(data);
    } else {
        d = this;
    }
    let seconds = d.getSeconds();
    let minutes = d.getMinutes();
    let hours = this.getUTCHours();
    let year = d.getFullYear();
    let month = d.getMonth() + 1;
    if (month.toString().length <= 1) {
        month = '0' + month;
    }
    // let day = ('0' + d.getDate()).slice(-2);
    let day = d.getDay();
    if (day.toString().length <= 1) {
        day = '0' + day;
    }
    return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
};
