const DEVELOP_MODE = true;
const MONGODB_DN_NAME = "accessControl";
const MONGODB_URL = "mongodb://localhost:27017/";

// const MONGODB_URL = 'mongodb://appsTeam:App$teaM@localhost:27017/';
// const MONGODB_URL = 'mongodb://160.99.37.225:27017/';

module.exports = {
    developedMode: DEVELOP_MODE,
    appsteamPortalHost: "http://portal.vtsappsteam.edu.rs/api/",
    appsteamPortalRouteWaitUnregisteredDevice: "sync/mac",
    appsteamPortalSendReportTimeInLab: "labtime",
    appsteamPortalGetTimeInLabForUser: "labtime/user",
    hostUrl: "160.99.37.225",
    appPort: 443,
    socketIOPort: 443,
    durationOpenDoor: 4000,
    fireBaseApiKey: "AIzaSyC0Af-W3nsobj2UjpLY_Mj902tYqZqZsfo",
    uniqueToken: "D/6H{x7ueK?%[w/2p4_G_*V",
    mongodbUrlDB: MONGODB_URL,
    mongodbName: MONGODB_DN_NAME,
    mongodbCollectionStudents: 'users',
    mongodbCollectionAvailable: 'available_users_in_lab',
    mongodbCollectionChat: 'chat',
    mongodbCollectionSettings: 'settings',
};