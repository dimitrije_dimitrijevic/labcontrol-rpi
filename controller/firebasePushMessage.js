const express = require('express'),
    constants = require("../controller/constants"),
    FCM = require('fcm-node');
const fcm = new FCM(constants.fireBaseApiKey);


// module.exports.fcmMessageCheckOut = (ip) => {
//     MongoClient.connect(urlDB, function (err, db) {
//         if (err && err.name === "MongoError") {
//             console.log("Error connected MongoDB");
//         }
//         else {
//             var cursor = db.collection(collectionPrisutni).find({
//                 "ipAdresa": ip
//             });
//             cursor.each(function (err, doc) {
//                 assert.equal(err, null);
//                 if (doc != null) {
//                     var searchFcmToken = db.collection(collectionStudenti).find({
//                         "macAdresa": doc.macAdresa
//                     });
//                     searchFcmToken.each(function (errSFT, docSFT) {
//                         assert.equal(errSFT, null);
//
//                         if (docSFT != null) {
//                             fcmSendMessage(docSFT.tokenFirebase, "Obavestenje", "Ukoliko niste u laboratoriji, odjavite se", 'Notification_Info');
//                             return false;
//                             db.close();
//                         } else {
//                             return false;
//                             db.close();
//                         }
//                     });
//                 } else {
//                     return false;
//                     db.close();
//                 }
//             });
//         }
//     });
// };
module.exports.fcmSendMessageDate = (messageFor, dataFor, titleFor, bodyFor, clickAction) => {

    let message = {
        to: messageFor,
        data: dataFor,
        notification: {
            title: titleFor,
            body: bodyFor,
            priority: 'High',
            sound: 'Enabled',
            click_action: clickAction
        }
    };
    fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: \n", response);
        }
    });
};

module.exports.fcmSendMessage = (messageFor, titleFor, bodyFor, clickAction) => {
    let message = {
        to: messageFor,
        notification: {
            title: titleFor,
            body: bodyFor,
            priority: 'High',
            sound: 'Enabled',
            click_action: clickAction
        }
    };
    fcm.send(message, function (err, response) {
        if (err) {
            console.log("Something has gone wrong!");
        } else {
            console.log("Successfully sent with response: \n", response);
        }
    });
};

