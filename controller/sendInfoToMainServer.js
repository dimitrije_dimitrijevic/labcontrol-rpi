const express = require('express'),
    constants = require("../controller/constants"),
    request = require('request'),
    MongoClient = require('mongodb').MongoClient,
    ObjectId = require('mongodb').ObjectID,
    assert = require('assert');

module.exports.sendUnregisteredDeviceWaitMode = (userDevice, callback) => {
    let send = (typeof callback === "function");
    request.post({
        headers: {'Content-Type': 'application/json'},
        url: constants.appsteamPortalHost + constants.appsteamPortalRouteWaitUnregisteredDevice,
        form: {
            uniqueToken: constants.uniqueToken,
            userDevice
        }
    }, function (err, res, body) {
        // console.log(err, res, body);
        try {
            assert.equal(err, null);
            MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
                try {
                    assert.equal(null, err);
                    const db = client.db(constants.mongodbName);

                    updateUserMacAddressDevice(db, userDevice, (err, results) => {
                        client.close();
                        if (send) {
                            callback(false, {err, res, body});
                        }
                    });
                } catch (err) {
                    if (send) {
                        callback(false, {err, res, body});
                    }
                }
            });
        } catch (err) {
            MongoClient.connect(constants.mongodbUrlDB, (err, client) => {
                try {
                    assert.equal(null, err);
                    const db = client.db(constants.mongodbName);

                    updateUserMacAddressDevice(db, userDevice, (err, results) => {
                        client.close();
                        if (send) {
                            callback(false, {err, res, body});
                        }
                    });
                } catch (err) {
                    if (send) {
                        callback(false, {err, res, body});
                    }
                }
            });
        }
    });
};

module.exports.getUserTimeInLab = (data, callback) => {
    request.post({
        headers: {'Content-Type': 'application/json'},
        url: constants.appsteamPortalHost + constants.appsteamPortalGetTimeInLabForUser,
        form: data
    }, function (err, res, body) {
        try {
            assert.equal(err, null);
            callback(false, JSON.parse(body))
        } catch (err) {
            callback(true, {err, res, body: JSON.parse(body)});
        }
    });
};

module.exports.sendTodayReportTimeInLab = (data, callback) => {
    if (data.length > 0) {
        request.post({
            headers: {'Content-Type': 'application/json'},
            url: constants.appsteamPortalHost + constants.appsteamPortalSendReportTimeInLab,
            form: {
                uniqueToken: constants.uniqueToken,
                timeInLab: data
            }
        }, function (err, res, body) {
            // console.log(err, res, body);
            // console.log(res.statusCode);
            // console.log(res.body);
            // console.log(body);
            try {
                assert.equal(err, null);
                if (res.statusCode === 200) {
                    if (JSON.parse(body).succes === "lab time added")
                        callback(false, {err, res, body});
                } else {
                    callback(true, {err, res, body});
                }
            } catch (err) {
                callback(true, {err, res, body})
            }
        });
    } else {
        callback(false, {message: "No Time Log"})
    }
};

const updateUserMacAddressDevice = function (db, user, callback) {
    const collection = db.collection(constants.mongodbCollectionStudents);
    collection.updateOne({username: user.username}
        , {$set: {macAddressDevice: user.macAddressDevice}}, function (err, result) {
            try {
                assert.equal(err, null);
                return callback(false, result);
            } catch (e) {
                return callback(true, result);
            }
        });
};